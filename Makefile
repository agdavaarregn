SHELL=/bin/bash


all: 
	cd doedssynderna; make; cd ..;
	cd lathund; make; cd ..;
	cd triangelstart; make; cd ..;
	cd trimguide; make; cd ..;

export: /tmp/agdavaarregn.zip
/tmp/agdavaarregn.zip: all /tmp/agdavaarregn LICENCE
	rsync -v {doedssynderna,lathund,triangelstart,trimguide}/*.pdf LICENCE /tmp/agdavaarregn
	cd /tmp; zip agdavaarregn.zip agdavaarregn/*; cd -;

/tmp/agdavaarregn:
	mkdir /tmp/agdavaarregn

clean:
	cd doedssynderna; make clean; cd ..;
	cd lathund; make clean; cd ..;
	cd triangelstart; make clean; cd ..;
	cd trimguide; make clean; cd ..;
	rm *~
