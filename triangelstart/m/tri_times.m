%% -- Function: tri_times (WIND)
%%
%%     Octave script for to generate triangle start times. Separate sets are
%%     used for light and heavy winds. WIND can be 'light' or 'hard'
%%
%%
%% speed     = [ first leg, second leg, final leg ]
%%
%% totTimes  = [ in seconds
%%               minute part
%%               second part ]
%%
%% xTimes    = [ time on first leg in seconds
%%               time on second leg
%%               time on final leg ]
%%
%% turnTimes = [ t_1 in seconds
%%               t_1 minute part
%%               t_1 second part 
%%               t_2 in seconds total
%%               t_2 minute part
%%               t_2 second part ]
%%

function tri_times (wind)
								% number of columns
  %% Parameters
  if (size (wind) == [1 5] && wind == "light")
	numCol = 2;                % number of columns
	printStyle = 2;            % larger value for verbosity
	totTimes = [280:-5:45];    % (s) total
	angDeg = [110 30 40];      % (degrees) angles oposit corresponding legs
	speedFactors = [1 0.5 1];  % speed on all legs ex: [1 0.5 1]
  elseif (size (wind) == [1 4] && wind == "hard")
	numCol = 2;                % number of columns
	printStyle = 2;            % larger value for verbosity
	totTimes = [280:-5:45];    % (s) total
	angDeg = [110 30 40];      % (degrees) angles oposit corresponding legs
	speedFactors = [1 1 1];  % speed on all legs ex: [1 0.5 1]
  else
	error ("Wrong argument");
	return;
  endif
  
  %% Perform the float calculations
  angRad = angDeg * pi / 180; % (rad)
  sumSinus = sin(angRad(1)) + sin(angRad(2)) + sin(angRad(3));
  sideFactors = sumSinus * [sin(angRad(1)) sin(angRad(2)) sin(angRad(3))];
  factor = 1 / sum(speedFactors .* sideFactors, 2);

  numEl = numel(totTimes);
  xTimes(1, :) = (totTimes(1, :) - 10) * factor * speedFactors(1) * sideFactors(1,1);
  xTimes(2, :) = (totTimes(1, :) - 10) * factor * speedFactors(2) * sideFactors(1,2);
  xTimes(3, :) = (totTimes(1, :) - 10) * factor * speedFactors(3) * sideFactors(1,3);
  turnTimes = totTimes(1, :) - xTimes(1, :);
  turnTimes(4, :) = turnTimes(1, :) - xTimes(2, :) - 10;

  %% Check calculations
  if (abs(sum(xTimes, 1) + 10 - totTimes(1,:)) > 2.2e-14)
	error ("Check sum error 1");
  endif
  if (abs(turnTimes(4, :) - xTimes(3, :)) > 2.2e-14)
	error ("Check sum error 2");
  endif

  %% Round the times and split into minutes and seconds. 
  totTimes(1, :) = round (totTimes(1,:));
  totTimes(3, :) = rem (totTimes(1, :), 60); % (s) second part
  totTimes(2, :) = round ((totTimes(1, :) - totTimes(3, :))/60); % (min) minute part
  turnTimes(1, :) = round (turnTimes(1,:));
  turnTimes(3, :) = rem (turnTimes(1, :), 60); % (s) second part
  turnTimes(2, :) = round ((turnTimes(1, :) - turnTimes(3,:))/60); % (min) minute part
  turnTimes(4, :) = round (turnTimes(4,:));
  turnTimes(6, :) = rem (turnTimes(4, :), 60); % (s) second part
  turnTimes(5, :) = round ((turnTimes(4, :) - turnTimes(6,:))/60); % (min) minute part
  xTimes = round (xTimes);

  %% Print the result as formated latex code
  numRow = ceil(numEl / numCol);

printf ("\\begin{tabular}{");
  for i = 1:numCol
	printf ("c");
  endfor
printf ("}\n");

  elem = 1;
  for i = 1:numCol

	switch printStyle
	  case 1
		printf ("\\begin{tabular}{r r}\n");
		printf ("$T$ & $t_1$\\\\\n\\hline\n");
	  case 2
		printf ("\\begin{tabular}{r | r r | r r r|}\n");
		printf ("$T$ & $t_1$ & $t_2$ & $s_1$ & $s_2$ & $s_3$ \\\\\n\\hline\n");
	  otherwise
		error ("Error 3");
	endswitch
	
	j = 1;
	while j <= numRow & elem <= numEl

	  switch printStyle
		case 1
		printf ("$%2.1d'\\,%2.2d''$ & $%2.1d'\\,%2.2d''$\\\\\n", ...
				  totTimes(2, elem), ...
				  totTimes(3, elem), ...
				  turnTimes(2, elem), ...
				  turnTimes(3, elem));
		case 2
		printf ("$\\mathbf{%2.1d'\\,%2.2d''}$ & $%2.1d'\\,%2.2d''$ & $%2.1d'\\,%2.2d''$ & ", ...
				  totTimes(2, elem), ...
				  totTimes(3, elem), ...
				  turnTimes(2, elem), ...
				  turnTimes(3, elem), ...
				  turnTimes(5, elem), ...
				  turnTimes(6, elem));
		printf ("$%3.1d''$ & $%3.1d''$ & $%3.1d''$ \\\\\n", ...
				  xTimes(1, elem), ...
				  xTimes(2, elem), ...
				  xTimes(3, elem));
		otherwise
		  error ("Error 3");
	  endswitch
      
	  if rem(j, 3) == 0
		printf ("\\hline\n");
	  endif
	  j = j + 1;
	  elem = elem + 1;
	endwhile

	printf ("\\end{tabular}%%\n");
	if i < numCol
	printf ("&%%\n");
	endif

  endfor
printf ("\\end{tabular}\n");
  
