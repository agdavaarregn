%% -- Function: tri_angles (VERSION)
%%
%%     Performs output of angle calculation. where VERSION is the string
%%     v2.0'.

function tri_angles (version)

  
  %% Parameter check
  if size (version) == [1 4]
	switch (version)
	  case {"v2.0"}
		numCol = 3;
		angles = [0:20:340];
		corners = [110 30 40];
	  otherwise
		error ("Unknown triangle version");
		return;
	endswitch
	if sum (corners) ~= 180
	  error ("Wrong sum of corners.");
	  return;
	endif
  else
	error ("Unknown triangle version");
	return;
  endif
  
  %% Derived parameters
  numEl = numel(angles);
  numRow = ceil(numEl / numCol);
  angles(2, 1:numEl) = angles(1, 1:numEl) + (180 - corners(3));
  angles(3, 1:numEl) = angles(2, 1:numEl) + (180 - corners(1));
  angles = rem(angles, 360);

  printf ("\\begin{tabular}{");
  for i = 1:numCol
	printf ("c");
  endfor
  printf ("}\n");

  elem = 1;
  for i = 1:numCol
	printf ("\\begin{tabular}{rrr}\n");
	printf (" $k$ & $k + %.1d^\\circ$ & $k + %.1d^\\circ$ \\\\\\hline\n", ...
			180 - corners(3),360 - corners(3) - corners(1) );

	j = 1;
	while j <= numRow & elem <= numEl
      printf ("$%3.1d^\\circ$ & $%3.1d^\\circ$ & $%3.1d^\\circ$ \\\\\n", ...
			  angles(1, elem), ...
			  angles(2, elem), ...
			  angles(3, elem));
      
      if rem(j, 3) == 0
		printf ("\\hline\n");
      endif
      j = j + 1;
      elem = elem + 1;
	endwhile

	printf ("\\end{tabular}%%\n");
	if i < numCol
      printf ("&%%\n");
	endif
	
  endfor
  printf ("\\end{tabular}\n");





