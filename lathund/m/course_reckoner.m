%% -- Function: course_reckoner ()
%%
%%     Generates a course reckoner.

function course_reckoner ()

  columns = -135:45:180;
  rows = 0:5:40;
  result = zeros (numel (rows), numel (columns));
  for i = 1:numel (rows)
	for j = 1:numel (columns)
	  result(i,j) = mod ( 3600 + rows(i) + columns(j), 360);
	endfor
  endfor
  
  %% print header row
  printf ("\\begin{tabular}{@{}");
  for i = 1:numel (columns)
	printf ("r");
  endfor
  printf ("@{}}\n");
  printf ("$%+3.1d^\\circ$", columns(1));
  for i = 2:numel (columns)
	printf (" & $%+3.1d^\\circ$", columns(i));
  endfor
  printf ("\\\\\\toprule\n");
  for i = 1:size (result, 1)
	if (rem (i - 1, 3) == 0 && i != 1)
	  printf ("\\\\\\midrule\n");
	else
	  printf ("\\\\[0.1em]\n");
	endif
	printf ("$%3.1d^\\circ$", result(i,1));
	for j = 2:size (result, 2)
	  printf (" & $%3.1d^\\circ$", result(i,j));
	endfor
  endfor
  printf ("\n\\end{tabular}\n");
	
	  

  

  

  

